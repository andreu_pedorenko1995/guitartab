Rails.application.config.middleware.use OmniAuth::Builder do
  provider :vkontakte, ENV['vk_key'], ENV['vk_secret'], scope: 'email'
  provider :facebook, ENV['facebook_key'], ENV['facebook_secret'], scope: 'email'
end