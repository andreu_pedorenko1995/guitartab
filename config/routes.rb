Rails.application.routes.draw do
  root 'welcome#show'
  get 'contact/show'
  get '/contact', to: 'contact#show'
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'

  resources :users do
    resources :profiles, only: [:edit, :update, :show]
  end
  resource :sessions, only: [:new, :create, :destroy] do
  end

  get 'auth/:provider/callback', to: 'omniauth#create'

  resources :tracks do
    resources :chords
  end

  namespace :admin do
    root 'sessions#new'
    get 'logout', to: 'sessions#destroy'
    get 'login', to: 'sessions#new', as: 'login'
    resources :users, only: [:index, :destroy]
    resources :sessions, only: [:new, :create, :destroy]
  end
end
