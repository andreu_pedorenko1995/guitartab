module ProfilesHelper
  def require_user_profile
    current_user == @profile.user
  end
end
