class User < ActiveRecord::Base
  validates_uniqueness_of :email

  has_secure_password
  has_secure_token

  has_one :profile, dependent: :destroy
  has_many :accounts, dependent: :destroy
  has_many :tracks
  has_many :chords

  before_create :build_profile
end
