class Admin::UsersController < AdminsController
  before_action :current_resourse, only: [:destroy]

  def index
    @users = User.all
  end

  def destroy
    @user.destroy
  end

  private

  def current_resourse
    @user = User.find(params[:id])
  end
end
