class ProfilesController < ApplicationController
  before_action :profile_resource

  def update
    @profile.update(profile_params)
    redirect_to user_profile_url
  end

  def show
    @user = User.find(params[:user_id])
  end

  private

  def profile_resource
    @profile = User.find(params[:user_id]).profile
  end

  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :birtday, :avatar)
  end
end
