class ChordsController < ApplicationController
  def create
    @track = Track.find(params[:track_id])
    @chord = @track.chords.create(chord_params)
    @chord.user_id = current_user.id
    @chord.save
  end

  def destroy
    @track = Track.find(params[:track_id])
    @chord = @track.chords.find(params[:id])
    @chord.destroy
  end

  private

  def chord_params
    params.require(:chord).permit(:user_id, :tablature)
  end
end
