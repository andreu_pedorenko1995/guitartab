class AdminsController < ApplicationController
  layout 'admin/application'

  before_action :require_admin

  private

  def require_admin
    redirect_to root_path unless session[:admin]
  end
end
