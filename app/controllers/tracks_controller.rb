class TracksController < ApplicationController
  before_action :current_track, only: [:show, :edit, :update, :destroy]
  skip_before_action :require_user

  def index
    @tracks = Track.all
  end

  def new
    @track = Track.new
  end

  def create
    @track = current_user.tracks.create(track_params)
    if @track.save
      puts "------>#{@track.user_id.class}"
      redirect_to @track
    else
      render 'new'
    end
  end

  def update
    if @track.update(track_params)
      redirect_to @track
    else
      render 'edit'
    end
  end

  def destroy
    track.destroy
    redirect_to tracks_path
  end

  private

  def track
    @track ||= current_user.tracks.find(params[:id])
  end

  def track_params
    params.require(:track).permit(:name, :user_id, :description)
  end

  def current_track
    @track = Track.find(params[:id])
  end
end
