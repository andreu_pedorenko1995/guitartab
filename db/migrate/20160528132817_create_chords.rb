class CreateChords < ActiveRecord::Migration
  def change
    create_table :chords do |t|
      t.belongs_to :track, index: true, foreign_key: true
      t.belongs_to :user, index: true, foreign_key: true
      t.text :tablature
      t.timestamps null: false
    end
  end
end
