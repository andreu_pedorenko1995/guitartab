class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :name
      t.belongs_to :user, index: true, foreign_key: true
      t.string :description

      t.timestamps null: false
    end
  end
end
